SHELL=bash
.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install: ## create a virtualenvironment in ./env and populate it
	@python3 -m venv ./env
	@source ./env/bin/activate ; pip install -r requirements.txt ; pip install -r requirements-git.txt

