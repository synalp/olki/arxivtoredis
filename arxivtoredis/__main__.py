#!/usr/bin/python

import json
import redis
import datetime
import os
import time
from collections import namedtuple
from pprint import pprint
import click
import arxivpy
from . import arxivscraper

redis_host = os.environ.get('REDIS_HOST', 'localhost')
redis_port = os.environ.get('REDIS_PORT', 6379)
redis_password = os.environ.get('REDIS_PASSWORD', '')

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=CONTEXT_SETTINGS)
@click.version_option(version='0.1.0')
def entry():
    pass


@entry.command()
@click.argument('category')
@click.argument('days', required=False, default=3)
def arxiv(**kwargs):
    if kwargs['category'] != 'all':
        get_papers(category=kwargs['category'], days=kwargs['days'])
    else:
        for category in arxivscraper.custom_categories:
            get_papers(category, days=kwargs['days'])
            time.sleep(10)  # the API requires a 10s temporisation
    process_raw()


@entry.command()
@click.argument('category', required=False)
def stats(**kwargs):
    if kwargs['category']:
        number_of_papers_for_category = getattr(get_statistics(), kwargs['category'].replace('-', '_'))
        print('{} papers in category {}'.format(number_of_papers_for_category, kwargs['category']))
        return number_of_papers_for_category
    raw, sorted, *_ = get_statistics()
    print("raw papers: {}\nsorted papers: {}".format(raw, sorted))


@entry.command()
def flush(**kwargs):
    delete_all()


def get_papers(category="cs", days=3):
    day = datetime.date.today()
    fromday = day-datetime.timedelta(days)

    start_index = 0
    max_index = 100
    res = arxivpy.query(search_query=category,
                        start_index=start_index, max_index=max_index, results_per_iteration=100,
                        wait_time=5.0, sort_by='lastUpdatedDate')
    while ((len(res) > 0) and (res[-1]['update_date'].date() > fromday)):
        start_index = max_index
        max_index += 100
        res += arxivpy.query(search_query=category,
                             start_index=start_index, max_index=max_index, results_per_iteration=100,
                             wait_time=5.0, sort_by='lastUpdatedDate')

    r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)
    new = 0
    for paper in res:
        dejavu = r.get("arxiv:raw:"+str(paper['id']))
        if dejavu is None:
            jsonstr = json.dumps(paper, cls=DateTimeEncoder)
            r.set("arxiv:raw:" + str(paper['id']), jsonstr)
            new += 1
    print("added {} new papers for query {}".format(str(new), category))


def process_raw():
    """processes raw entries to sort them in a new key"""
    r = redis.StrictRedis(host=redis_host, port=redis_port,
                          password=redis_password, decode_responses=True)
    papers = []

    for k in r.scan_iter("arxiv:raw:*"):
        papstr = r.get(k)
        papjson = json.loads(papstr)
        d = papjson['update_date']
        if (len(d) == 0):
            d = papjson['publish_date']
        papjson['day'] = d
        papers.append(papjson)

    def _extract_day(json):
        try:
            day = json['day'].split('T')[0]
            date = datetime.datetime.strptime(day, "%Y-%m-%d")
            # sort first by date, and then by ID
            return (date, json['id'])
        except KeyError:
            return 0

    # storing ordered list of all ids to find sorted papers easily
    papers_sorted = sorted(papers, key=_extract_day, reverse=True)
    s = [p['id'] for p in papers_sorted]
    ss = ' '.join(s)
    r.set("arxiv:sortedids", ss)

    # storing ordered list of all ids per category to find sorted papers by category easily
    for category in arxivscraper.custom_categories:
        s = [p['id'] for p in papers_sorted
             if category in
             [e.split('.')[0] for e in p['terms'].split(' ')]  # we are only interested in 'math' or 'physics' in what could be 'math.PR physics.pop-ph'
             ]
        ss = ' '.join(s)
        r.set("arxiv:sortedids:" + category, ss)


def delete_all(filter='*'):
    """delete all entries, with an optional filter"""
    r = redis.StrictRedis(host=redis_host, port=redis_port,
                          password=redis_password, decode_responses=True)

    for key in r.scan_iter("arxiv:" + filter):
        r.delete(key)


def get_statistics():
    """simple stats about the number of keys in store"""
    r = redis.StrictRedis(host=redis_host, port=redis_port,
                          password=redis_password, decode_responses=True)

    Statistics = namedtuple(
        'Statistics', ['raw', 'sorted', *[x.replace('-', '_') for x in arxivscraper.custom_categories]]
    )

    return Statistics(
            sum(1 for _ in r.scan_iter(match="arxiv:raw:*")),
            sum(1 for _ in r.get("arxiv:sortedids").split(' ')) if r.get("arxiv:sortedids") else 0,
            *[sum(1 for _ in r.get("arxiv:sortedids:" + category).split(' '))
              if r.get("arxiv:sortedids:" + category) else 0
              for category in arxivscraper.custom_categories],
           )


def next_for_user(user: str) -> str:
    r = redis.StrictRedis(host=redis_host, port=redis_port,
                          password=redis_password, decode_responses=True)
    read = r.get("arxiv:user:" + user)
    if read is None:
        rr = []
    else:
        rr = read.strip().split()
    ss = r.get("arxiv:sortedids")
    paps = ss.strip().split()
    to_read = []
    for p in paps:
        if p not in rr:
            to_read.append(p)
            if len(to_read) >= 4:
                break
    res = ""
    if len(to_read) > 0:
        for p in to_read:
            s = r.get("arxiv:raw:" + p)
            papjson = json.loads(s)
            d = papjson['updated']
            if (len(d) == 0):
                d = papjson['created']
            res += papjson['url'] + " " + str(d) + " " + papjson['title'] + '\n'
            rr.append(p)
        r.set("arxiv:user:" + user, ' '.join([str(x) for x in rr]))
    return res


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.isoformat()

        return json.JSONEncoder.default(self, o)


if __name__ == '__main__':
    entry(prog_name='arxivtoredis')

